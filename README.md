# Changing the log4j 2 Version

NSO use Apache Log4j 2 for Java log messages. This simple shell script help
change the version of the Log4j 2 jar files that are included with the NSO
release.

## Steps

1. Either source the ncsrc environment command file or run the script with the
   -h flag to get information on how to set the path to the jar file directory.
2. If necessary, make the script executable. E.g. `chmod u+x ./nso_log4j_2_upgrade.sh`
3. Run the script.
4. Restart running NSO, netsim, and application instances that use the log4j 2 binary.

## Requirements

The curl and gpg commands need to be installed.

## Examples

Get help
```
./nso_log4j_2_upgrade.sh -h
```
To, for example, upgrade to 2.20.0 using the default NSO paths:
```
source ncsrc; ./nso_log4j_2_upgrade.sh -v 2.20.0
```
Upgrade to 2.20.0 specifying the NSO system installation paths:
```
./nso_log4j_2_upgrade.sh -v 2.20.0 -p /opt/ncs/ncs-5.7/java/jar -n /opt/ncs/ncs-5.7/netsim/confd/java/jar -m /opt/ncs/ncs-5.7/lib/ncs/lib/core/sls/priv/smartagent/WEB-INF/lib
```

