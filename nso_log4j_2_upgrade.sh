#!/bin/bash
set -eu # Abort the script if a command returns with a non-zero exit code or if
        # a variable name is dereferenced when the variable hasn't been set

RED='\033[0;31m'
GREEN='\033[0;32m'
PURPLE='\033[0;35m'
NC='\033[0m' # No Color

NEW_VERSION=2.20.0 # Default version
LOG4J_2_URL="https://dlcdn.apache.org/logging/log4j/$NEW_VERSION/apache-log4j-$NEW_VERSION-bin.tar.gz"
KEYS_URL="https://downloads.apache.org/logging/KEYS"
SIGN_URL="https://downloads.apache.org/logging/log4j/$NEW_VERSION/apache-log4j-$NEW_VERSION-bin.tar.gz.asc"

function usage()
{
   printf "${GREEN}Script for upgrading the log4j 2 version used by the NSO Java API\n\n"
   printf "  -v  New Log4j 2 version. Default: $NEW_VERSION\n"
   printf "  -u  URL to the Apache Log4j 2 binary (tar.gz). Default: https://dlcdn.apache.org/logging/log4j/NEW_VERSION/apache-log4j-NEW_VERSION-bin.tar.gz\n"
   printf "  -p  Path to the NSO Java API jar files. Default: \$NCS_DIR/java/jar\n"
   printf "  -n  Path to the netsim ConfD Java API jar files. Default: \$NCS_DIR/netsim/confd/java/jar\n"
   printf "  -m  Path to the NSO Smart agent jar files. No Default: \$NCS_DIR/lib/ncs/lib/core/sls/priv/smartagent/WEB-INF/lib\n"
   printf "  -k  URL to the KEYS for verifying the integrity of the Apache Log4j 2 distribution. Default: $KEYS_URL\n"
   printf "  -s  URL to the asc signature file for verifying the integrity of the Apache Log4j 2 distribution. Default: https://downloads.apache.org/logging/log4j/NEW_VERSION/apache-log4j-NEW_VERSION-bin.tar.gz.asc\n"
   printf "\nExample: Upgrade a local install to $NEW_VERSION:\n\n"
   printf "  \$ source ncsrc; ./nso_log4j_2_upgrade.sh -v $NEW_VERSION\n\n"
   printf "Example: Upgrade a system install to $NEW_VERSION:\n\n"
   printf "  \$ ./nso_log4j_2_upgrade.sh -v $NEW_VERSION -p /opt/ncs/ncs-5.7/java/jar -n /opt/ncs/ncs-5.7/netsim/confd/java/jar -m /opt/ncs/ncs-5.7/lib/ncs/lib/core/sls/priv/smartagent/WEB-INF/lib\n\n${NC}"
}

# Retrieve the calling parameters.
while getopts "v:u:p:n:m:k:s:h" OPTION; do
    case "${OPTION}"
    in
        v)  NEW_VERSION="${OPTARG}";;
        u)  LOG4J_2_URL="${OPTARG}";;
        p)  JAR_PATH="${OPTARG}";;
        n)  NETSIM_JAR_PATH="${OPTARG}";;
        m)  SMARTAGENT_JAR_PATH="${OPTARG}";;
        k)  KEYS_URL="${OPTARG}";;
        s)  SIGN_URL="${OPTARG}";;
        h)  usage; exit 0;;
        \?) printf "${RED}Invalid parameter${NC}"; usage; return 1;;
    esac
done

set +u
if [ -z "$JAR_PATH" ]; then
    if ! [ -z "${NCS_DIR}" ]; then
        JAR_PATH=${NCS_DIR}/java/jar
    else
        printf "${RED}Path to the NSO Java API jar files or \$NCS_DIR is not set. Aborting.\n${NC}"; usage; exit 1;
    fi
fi
if [ -z "$NETSIM_JAR_PATH" ]; then
    if ! [ -z "${NCS_DIR}" ]; then
        NETSIM_JAR_PATH=${NCS_DIR}/netsim/confd/java/jar
    else
        printf "${RED}Path to the netsim ConfD Java API jar files or \$NCS_DIR is not set. Aborting.\n${NC}"; usage; exit 1;
    fi
fi
if [ -z "$SMARTAGENT_JAR_PATH" ]; then
    if ! [ -z "${NCS_DIR}" ]; then
        SMARTAGENT_JAR_PATH=${NCS_DIR}/lib/ncs/lib/core/sls/priv/smartagent/WEB-INF/lib
    else
        printf "${RED}Path to the NSO Smart agent Java API jar files or \$NCS_DIR is not set. Aborting.\n${NC}"; usage; exit 1;
    fi
fi
set -u

hash curl 2>/dev/null || { printf "${RED}curl not installed. Aborting.${NC}\n"; exit 1; }
hash gpg 2>/dev/null || { printf "${RED}gpg not installed. Aborting.${NC}\n"; exit 1; }

printf "\n${PURPLE}Change directory to $JAR_PATH\n${NC}"
cd $JAR_PATH
printf "\n${PURPLE}Remove the old log4j 2 jar files\n${NC}"
rm -fv log4j-*.jar
rm -fv $NETSIM_JAR_PATH/log4j-*.jar
rm -fv $SMARTAGENT_JAR_PATH/log4j-*.jar
printf "\n${PURPLE}Download keys, signature, and the log4j 2 $NEW_VERSION release from $LOG4J_2_URL\n${NC}"
curl -s $KEYS_URL --output KEYS
curl -s $SIGN_URL --output apache-log4j-$NEW_VERSION-bin.tar.gz.asc
curl -s $LOG4J_2_URL --output apache-log4j-$NEW_VERSION-bin.tar.gz
printf "\n${PURPLE}Use gpg to verify the integrity of the Apache Log4j 2 distribution\n${NC}"
gpg -v --import KEYS
gpg -v --verify apache-log4j-$NEW_VERSION-bin.tar.gz.asc apache-log4j-$NEW_VERSION-bin.tar.gz
printf "\n${PURPLE}Extract the Apache Log4j 2 distribution tarball\n${NC}"
tar xvfz apache-log4j-$NEW_VERSION-bin.tar.gz
printf "\n${PURPLE}Replace the existing log4j 2 core and api jar files with the new $NEW_VERSION version\n${NC}"
cp -vf ./apache-log4j-$NEW_VERSION-bin/log4j-slf4j-impl-$NEW_VERSION.jar .
cp -vf ./apache-log4j-$NEW_VERSION-bin/log4j-core-$NEW_VERSION.jar .
cp -vf ./apache-log4j-$NEW_VERSION-bin/log4j-api-$NEW_VERSION.jar .
cp -vf log4j-core-$NEW_VERSION.jar log4j-core.jar
cp -vf log4j-api-$NEW_VERSION.jar log4j-api.jar
cp -vf ./apache-log4j-$NEW_VERSION-bin/log4j-slf4j-impl-$NEW_VERSION.jar $NETSIM_JAR_PATH
cp -vf ./apache-log4j-$NEW_VERSION-bin/log4j-core-$NEW_VERSION.jar $NETSIM_JAR_PATH
cp -vf ./apache-log4j-$NEW_VERSION-bin/log4j-api-$NEW_VERSION.jar $NETSIM_JAR_PATH
cp -vf $NETSIM_JAR_PATH/log4j-core-$NEW_VERSION.jar $NETSIM_JAR_PATH/log4j-core.jar
cp -vf $NETSIM_JAR_PATH/log4j-api-$NEW_VERSION.jar $NETSIM_JAR_PATH/log4j-api.jar
cp -vf ./apache-log4j-$NEW_VERSION-bin/log4j-api-$NEW_VERSION.jar $SMARTAGENT_JAR_PATH
cp -vf ./apache-log4j-$NEW_VERSION-bin/log4j-to-slf4j-$NEW_VERSION.jar $SMARTAGENT_JAR_PATH
printf "\n${PURPLE}Cleanup\n${NC}"
rm -rvf ./apache-log4j-$NEW_VERSION-bin*
rm -vf KEYS
printf "\n${GREEN}Done\n${NC}"
